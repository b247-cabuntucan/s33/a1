    /*
    [done]3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API. 
    [done]4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
    [done]5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
    [done]6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
    [done] 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
    [done]8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
    [done]9. Update a to do list item by changing the data structure to contain the following properties:
    Title
    Description
    Status
    Date Completed
    User ID
    [done]10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
    [done]11. Update a to do list item by changing the status to complete and add a date when the status was changed.
    [done]12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
    [done]13. Create a request via Postman to retrieve all the to do list items.
    GET HTTP method
    https://jsonplaceholder.typicode.com/todos URI endpoint
    Save this request as get all to do list items
    [done]14. Create a request via Postman to retrieve an individual to do list item.
    GET HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as get to do list item
    [done]15. Create a request via Postman to create a to do list item.
    POST HTTP method
    https://jsonplaceholder.typicode.com/todos URI endpoint
    Save this request as create to do list item
    [done]16. Create a request via Postman to update a to do list item.
    PUT HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as update to do list item PUT
    Update the to do list item to mirror the data structure used in the PUT fetch request
    [done]17. Create a request via Postman to update a to do list item.
    PATCH HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as create to do list item
    Update the to do list item to mirror the data structure of the PATCH fetch request
    [done]18. Create a request via Postman to delete a to do list item.
    DELETE HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as delete to do list item
    19. Export the Postman collection and save it in the activity folder.
    20. Create a git repository named s33.
    */

// output 1
fetch('https://jsonplaceholder.typicode.com/todos/')
.then(response => response.json())
.then(data => console.log(data))

//output 2
fetch('https://jsonplaceholder.typicode.com/todos/')
.then(response => response.json())
.then(data => data.map(item => console.log(item.title)))

//output 3
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => console.log(`Todo Name: ${data.title}, status completed: ${data.completed}`))

//output 4
fetch('https://jsonplaceholder.typicode.com/todos/', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Complete the web dev course',
        completed: false
    })
})
.then(response => response.json())
.then(data => console.log(data))

//output 5
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'update todo',
        completed: true
    })
})
.then(response => response.json())
.then(data => console.log(data))

//output 6
fetch('https://jsonplaceholder.typicode.com/todos/2', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Eat Dinner',
        decription: 'Eating dinner is important',
        completed: true,
        date_completed: '2023-2-22',
        user_id: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))

//output 7
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Complete the react js course',
        completed: false
    })
})
.then(response => response.json())
.then(data => console.log(data))

//output 8
fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        completed: true,
        date_completed: '2022-2-22'
    })
})
.then(response => response.json())
.then(data => console.log(data))

//output 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE',
})


